<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginTest extends WebTestCase
{
    public function testSuccessfull(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();

        $submitButton = $crawler->selectButton('Connexion');
        $form = $submitButton->form();
        $form["email"] = "john@test.fr";
        $form["password"] = 'abcd1234';

        $client->submit($form);

        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertRouteSame('app_calculator');

        $this->assertResponseIsSuccessful();
    }


    /**
     * @dataProvider provideFailed
     */
    public function testFailed(array $formData, string $errorMessage)
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/login');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $form = $crawler->filter("form[id=login-form]")->form($formData);

        $client->submit($form);

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $client->followRedirect();

        $this->assertSelectorTextContains('html', $errorMessage);
    }

    /**
     * @return Generator
     */
    public function provideFailed(): Generator
    {
        yield [
            [
                "email" => "email+1@email.com",
                "password" => 'fail'
            ],
            'Identifiants invalides.'
        ];

        yield [
            [
                "email" => "",
                "password" => ''
            ],
            'Identifiants invalides.'
        ];


    }
}

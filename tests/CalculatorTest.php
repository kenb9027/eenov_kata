<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CalculatorTest extends WebTestCase
{
    public function testBadMethodRequest(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneBy([]);
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame("app_calculator");
        $this->assertSelectorTextContains('h1', 'Simulateur CA');

        $client->request('GET', '/calculator/submit', 
        [
            'surface' => '25',
            'price' => '1500',
            'private' => false,
            'social' => true
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testUnauthorizedRequest(): void
    {
        $client = static::createClient();
        $client->request('POST', '/calculator/submit', 
        [
            'surface' => '25',
            'price' => '1500',
            'private' => false,
            'social' => true
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }

    public function testForbiddenRequest(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneBy([]);
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame("app_calculator");
        $this->assertSelectorTextContains('h1', 'Simulateur CA');

        $client->request('POST', '/calculator/submit', 
        [
            'surface' => '25',
            'price' => '1500',
            'private' => false,
            'social' => true
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    // TODO : fix bugs with csrfToken.
    // public function testSuccessfullRequest(): void
    // {
    //     $client = static::createClient();
    //     $userRepository = static::getContainer()->get(UserRepository::class);
    //     $testUser = $userRepository->findOneBy([]);
    //     $client->loginUser($testUser);

    //     $crawler = $client->request('GET', '/');
    //     $this->assertResponseIsSuccessful();
    //     $this->assertRouteSame("app_calculator");
    //     $this->assertSelectorTextContains('h1', 'Simulateur CA');

    //     $csrfTokenMock = $this->createMock(CsrfTokenManagerInterface::class);
    //     $csrfToken = $csrfTokenMock->getToken('csrf_token_calculator')->getValue();
    //     $client->request('POST', '/calculator/submit', 
    //     [
    //         'surface' => '25',
    //         'price' => '1500',
    //         'private' => false,
    //         'social' => true,
    //         'token' => $csrfToken
    //     ]);

    //     $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    // }
}

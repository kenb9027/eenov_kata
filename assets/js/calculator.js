function formatNumber(number) {
    let numberStr = number.toString();
    numberStr = numberStr.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return numberStr;
}

function createFrom() {
    const formContainer = document.getElementById("calculator-container");

    //Get datas from controller
    const surfaceControllerValue = parseFloat(
        formContainer.getAttribute("data-surface-value")
    );
    const priceControllerValue = parseFloat(
        formContainer.getAttribute("data-price-value")
    );
    const socialControllerValue =
        formContainer.getAttribute("data-social-value");
    const privateControllerValue =
        formContainer.getAttribute("data-private-value");
    const csrfToken = formContainer.getAttribute("data-csrf-value");

    //Form
    const form = document.createElement("form");
    form.id = "calculator-form";
    form.action = "/calculator/submit";
    form.method = "POST";

    // CSRF
    const csrfInput = document.createElement("input");
    csrfInput.id = "csrf";
    csrfInput.type = "hidden";
    csrfInput.name = "csrf_token_calculator"; // Remplacez par le nom de votre champ CSRF Symfony
    csrfInput.value = csrfToken;
    form.appendChild(csrfInput);

    // form.appendChild(document.createElement("br"));

    //Surface
    const surfaceLabel = document.createElement("label");
    surfaceLabel.textContent = "Surface habitable en m² :";
    surfaceLabel.className = "fw-bold mb-1";
    form.appendChild(surfaceLabel);
    form.appendChild(document.createElement("br"));

    const surfaceInput = document.createElement("input");
    surfaceInput.type = "number";
    surfaceInput.id = "surface";
    surfaceInput.name = "surface";
    surfaceInput.required = true;
    surfaceInput.min = 0;
    surfaceInput.value = surfaceControllerValue;
    surfaceInput.className = "form-control w-50 mb-1";
    form.appendChild(surfaceInput);

    // form.appendChild(document.createElement("br"));

    // Price
    const priceLabel = document.createElement("label");
    priceLabel.textContent = "Prix /m² TTC :";
    priceLabel.className = "fw-bold mb-1";
    form.appendChild(priceLabel);
    form.appendChild(document.createElement("br"));

    const priceInput = document.createElement("input");
    priceInput.type = "number";
    priceInput.id = "price";
    priceInput.name = "price";
    priceInput.required = true;
    priceInput.min = 0;
    priceInput.value = priceControllerValue;
    priceInput.className = "form-control w-50 mb-1";
    form.appendChild(priceInput);

    // form.appendChild(document.createElement("br"));

    //Fiscalité label
    const fiscaliteLabel = document.createElement("div");
    fiscaliteLabel.innerHTML = "Fiscalité";
    fiscaliteLabel.className = "fw-bold mb-1";
    form.appendChild(fiscaliteLabel);

    // Social
    const socialLabel = document.createElement("label");
    socialLabel.textContent = "Sociale ";
    socialLabel.className = "mx-2";
    form.appendChild(socialLabel);

    const socialInput = document.createElement("input");
    socialInput.type = "checkbox";
    socialInput.id = "social";
    socialInput.name = "social";
    socialInput.className = "form-check-input";
    socialInput.checked = socialControllerValue;
    form.appendChild(socialInput);

    form.appendChild(document.createElement("br"));

    // Private
    const privateLabel = document.createElement("label");
    privateLabel.textContent = "Privé ";
    privateLabel.className = "mx-2";
    form.appendChild(privateLabel);

    const privateInput = document.createElement("input");
    privateInput.type = "checkbox";
    privateInput.id = "private";
    privateInput.name = "private";
    privateInput.className = "form-check-input";
    privateInput.checked = privateControllerValue;
    form.appendChild(privateInput);

    form.appendChild(document.createElement("br"));

    // CA label
    const caResultLabel = document.createElement("div");
    caResultLabel.id = "ca-label";
    caResultLabel.className = "fw-bold mt-3 mb-1";
    caResultLabel.innerHTML = "Chiffre d'affaires TTC";
    form.appendChild(caResultLabel);

    //CA result
    const caResult = document.createElement("div");
    caResult.id = "ca-result";
    caResult.className = "fw-bold mb-1";
    caResult.innerHTML = "€";
    if (!isNaN(surfaceControllerValue) && !isNaN(priceControllerValue)) {
        caValue = surfaceControllerValue * priceControllerValue;
        caResult.innerHTML = formatNumber(caValue.toFixed(2)*1) + " €";
    }
    form.appendChild(caResult);

    formContainer.appendChild(form);

    // Event listener
    form.addEventListener("change", function (e) {
        e.preventDefault();
        let surface = parseFloat(document.getElementById("surface").value);
        let price = parseFloat(document.getElementById("price").value);
        let social = document.getElementById("social").checked;
        let private = document.getElementById("private").checked;
        let csrf = document.getElementById("csrf").value;

        const caResult = document.getElementById("ca-result");
        if (!isNaN(surface) && !isNaN(price)) {
            let caValue = surface * price;
            caResult.innerHTML = formatNumber(caValue.toFixed(2)*1) + " €";
        }
        if (isNaN(surface) || isNaN(price) || surface < 0 || price < 0) {
            caResult.innerHTML = "€";
        }

        //don't accept negative numbers
        if (isNaN(surface) || surface < 0) {
            surface = -1;
        }
        if (isNaN(price) || price < 0) {
            price = -1;
        }

        //*AJAX REQUEST
        const xhr = new XMLHttpRequest();
        xhr.open("POST", form.action, true);
        xhr.setRequestHeader(
            "Content-Type",
            "application/x-www-form-urlencoded"
        );

        // data to send
        const data =
            "surface=" +
            surface +
            "&price=" +
            price +
            "&social=" +
            social +
            "&private=" +
            private +
            "&token=" +
            csrf;
        // response handler
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log("Réponse du serveur :", xhr.responseText);
            }
        };

        xhr.send(data);
    });
}

createFrom();

console.log("Calculator.js loaded.");

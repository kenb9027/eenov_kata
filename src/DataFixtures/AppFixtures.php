<?php

namespace App\DataFixtures;

use App\Entity\Calculator;
use App\Entity\User;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $userPasswordHasherInterface;

    public function __construct (UserPasswordHasherInterface $userPasswordHasherInterface) 
    {
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('john@test.fr');
        $user->setPassword(
            $this->userPasswordHasherInterface->hashPassword(
                $user,
                'abcd1234'
            )
        );
        $user->setCreatedAt(new DateTime('now', new DateTimeZone("Europe/Paris") ));
        $manager->persist($user);


        $calculator = new Calculator();
        $calculator->setUser($user);
        $manager->persist($calculator);

        $manager->flush();
    }
}

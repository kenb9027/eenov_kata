<?php

namespace App\Controller;

use App\Entity\Calculator;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CalculatorController extends AbstractController
{
    private EntityManagerInterface $em;
    private CsrfTokenManagerInterface $csrfTokenManager;

    public function __construct(EntityManagerInterface $em, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->em = $em;
        $this->csrfTokenManager = $csrfTokenManager;
    }


    #[Route('/', name: 'app_calculator')]
    public function index(): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $calculator = $this->em->getRepository(Calculator::class)->findOneBy([
            'user' => $this->getUser()
        ]);

        $csrfToken = $this->csrfTokenManager->getToken('csrf_token_calculator')->getValue();

        return $this->render('calculator/index.html.twig', [
            'calculator' => $calculator,
            'csrf_token' => $csrfToken
        ]);
    }

    #[Route('/calculator/submit', name: 'app_calculator_submit', methods:['POST', 'PUT'])]
    public function submit(Request $request): Response
    {
        if (!$this->getUser()) {
            return new Response('Authentication required.', 401);
        }

        $submittedToken = $request->request->get('token');
        if (!$this->csrfTokenManager->isTokenValid(new CsrfToken('csrf_token_calculator', $submittedToken))) {
            return new Response('Invalid or missing CSRF token.', 403);
        }

        try {
            /** @var Calculator $calculator */
            $calculator = $this->em->getRepository(Calculator::class)->findOneBy([
                'user' => $this->getUser()
            ]);

            $surface = (float)$request->request->get('surface');
            $price = (float)$request->request->get('price');
            $social = $request->request->getBoolean('social');
            $private = $request->request->getBoolean('private');

            $dataSurface = $surface < 0 ? null : $surface;
            $dataPrice = $price < 0 ? null : $price;

            $calculator->setSurface($dataSurface);
            $calculator->setPrice($dataPrice);
            $calculator->setSocial($social);
            $calculator->setPrivate($private);
            $calculator->setUpdatedAt(new DateTime('now', new DateTimeZone("Europe/Paris") ));

            $this->em->persist($calculator);
            $this->em->flush();

            return new Response('Calculator updated.', 200);
        } catch (\Throwable $th) {
            //throw $th;
            return new Response('An error occured on updating calculator.', 500);
        }
    }
}

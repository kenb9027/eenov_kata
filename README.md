# KATA - SIMULATEUR DE CA

- PHP 8.1
- Symfony 6.3
- MySQL 5.4

## Dépendances

Lancer les commandes suivantes pour installer les dépendances :

- ``` composer install ```
- ``` npm install ```

## phpMyAdmin

Créer un utilisateur (avec tous les privilèges) :
- Nom :  `eenov`
- Pwd : `kataeenov`

## BDD
Lancer les commandes suivantes dans la console pour créer la base de données et les tables : 

- ``` symfony doctrine:database:create ```
- ``` symfony console doctrine:migrations:migrate ```

Lancer cette commande pour charger les fixtures :
- ``` symfony console doctrine:fixtures:load ```

Pour se connecter avec le compte créé par les fixtures, utilisez ces identifiants :
- Email : john@test.fr
- Pwd : abcd1234

## Webpack Encore
Lancer cette commande dans la console pour builder les fichiers de styles et les fichiers Javascript
- ```npm run dev```

## Tests - phpUnit
Lancer ces commandes pour créer une base de données de test et charger les fixtures sur celle-ci :
- ```symfony console doctrine:database:create --env=test```
- ```symfony console doctrine:migrations:migrate -n --env=test```
- ``` symfony console doctrine:fixtures:load --env=test ```

Lancer cette commande dans la console pour effectuer les test unitaire
- ``` symfony php bin/phpunit ```